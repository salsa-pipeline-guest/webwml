#use wml::debian::template title="Информация о безопасности LTS" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Lev Lamberov"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Как поддерживать безопасность вашей системы Debian LTS</toc-add-entry>

<p>Чтобы получать последние рекомендации Debian LTS по безопасности, подпишитесь на
список рассылки <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.</p>

<p>Более подробную информацию о проблемах безопасности в Debian см. на
странице <a href="../../security">Информация о безопасности Debian</a>.</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Последние рекомендации по безопасности</toc-add-entry>

<p>Эти страницы содержат архив рекомендаций по безопасности, отправленных в
список рассылки <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Рекомендации по безопасности Debian LTS (только заголовки)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Рекомендации по безопасности Debian LTS (резюме)" href="dla-long">
:#rss#}
<p>Последние рекомендации по безопасности Debian LTS также доступны в
<a href="dla">формате RDF</a>. Мы также предлагаем
<a href="dla-long">второй файл</a>, включающий первый абзац
этих рекомендаций по безопасности. Из него вы легко сможете понять, чего касается каждая из
них.</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Также доступны старые рекомендации по безопасности:
<:= get_past_sec_list(); :>

<p>Дистрибутивы Debian не являются уязвимыми ко всем проблемам безопасности.
<a href="https://security-tracker.debian.org/">Система отслеживания безопасности Debian</a>
собирает всю информацию о статусе уязвимостей в пакетах Debian. Уязвимости в ней
могут быть найдены по идентификатору CVE или по имени пакета.</p>


<toc-add-entry name="contact">Контактная информация</toc-add-entry>

<p>Пожалуйста, перед тем, как связаться с нами, прочтите
<a href="https://wiki.debian.org/LTS/FAQ">ЧАВО от команды Debian LTS</a>. Может быть,
там уже содержится ответ на ваш вопрос!</p>

<p>В том же ЧАВО можно найти <a href="https://wiki.debian.org/LTS/FAQ">\
контактную информацию</a>.</p>
