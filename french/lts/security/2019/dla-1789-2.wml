#use wml::debian::translation-check translation="460d9ab5fb178b5ea9668ed740490c9e8a330016" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>DLA-1789-1 fournissait le microcode mis à jour de CPU pour la plupart des
microprocesseurs d’Intel pour réduire les vulnérabilités matérielles de MSBDS,
MFBDS, MLPDS et MDSUM.</p>

<p>Cette mise à jour fournit une prise en charge supplémentaire pour quelques
CPU de serveur Sandybridge and Core-X non couverts dans la publication originale
du mois de mai. Pour une liste des modèles particuliers de CPU pris en charge
désormais, veuillez vous référer aux entrées listées sous CPUID 206D6 et 206D7 dans
<a href="https://www.intel.com/content/dam/www/public/us/en/documents/corporate-information/SA00233-microcode-update-guidance_05132019.pdf">https://www.intel.com/content/dam/www/public/us/en/documents/corporate-information/SA00233-microcode-update-guidance_05132019.pdf</a>
</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.20190618.1~deb8u1 du paquet intel-microcode.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de intel-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1789-2.data"
# $Id: $
