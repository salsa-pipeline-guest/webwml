<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various security issues were discovered in Graphicsmagick, a collection
of image processing tools.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18219">CVE-2017-18219</a>

    <p>An allocation failure vulnerability was found in the function
    ReadOnePNGImage in coders/png.c, which allows attackers to cause a
    denial of service via a crafted file that triggers an attempt at a
    large png_pixels array allocation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18220">CVE-2017-18220</a>

    <p>The ReadOneJNGImage and ReadJNGImage functions in coders/png.c allow
    remote attackers to cause a denial of service or possibly have
    unspecified other impact via a crafted file, a related issue
    to <a href="https://security-tracker.debian.org/tracker/CVE-2017-11403">CVE-2017-11403</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18229">CVE-2017-18229</a>

    <p>An allocation failure vulnerability was found in the function
    ReadTIFFImage in coders/tiff.c, which allows attackers to cause a
    denial of service via a crafted file, because file size is not
    properly used to restrict scanline, strip, and tile allocations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18230">CVE-2017-18230</a>

    <p>A NULL pointer dereference vulnerability was found in the function
    ReadCINEONImage in coders/cineon.c, which allows attackers to cause
    a denial of service via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18231">CVE-2017-18231</a>

    <p>A NULL pointer dereference vulnerability was found in the function
    ReadEnhMetaFile in coders/emf.c, which allows attackers to cause
    a denial of service via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9018">CVE-2018-9018</a>

    <p>There is a divide-by-zero error in the ReadMNGImage function of
    coders/png.c. Remote attackers could leverage this vulnerability to
    cause a crash and denial of service via a crafted mng file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u19.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1322.data"
# $Id: $
