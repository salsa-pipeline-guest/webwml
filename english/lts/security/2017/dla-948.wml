<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was found in Dropbear, a lightweight SSH2 server
and client.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9079">CVE-2017-9079</a>

     <p>Jann Horn discovered a local information leak in parsing the
     .authorized_keys file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
2012.55-1.3+deb7u2.</p>

<p>We recommend that you upgrade your dropbear packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-948.data"
# $Id: $
